import Grid from '@mui/system/Unstable_Grid';
import TaskList from './components/TaskContainer';
import TaskAdd from './components/TaskAdd';
import { Box } from '@mui/system';
import { Provider } from 'react-redux';
import { store } from './lib/store';
import "./App.css"

function App() {
  return (
    <Provider store={store}>
      <Box>
        <Box sx={{ maxWidth: "1200px", mx: "auto" }}>
          <Grid container spacing={5} sx={{ width: "100%" }}>
            <Grid xl={4} lg={4} md={6} xs={12}>
              <TaskAdd />
            </Grid>
            <Grid xl={8} lg={8} md={6} xs={12}>
              <TaskList />
            </Grid>
          </Grid>
        </Box>
      </Box>
    </Provider>
  );
}

export default App;
