import { Button } from '@mui/material';
import { TextField } from '@mui/material'
import { Box } from '@mui/system'
import React, { useState } from 'react'
import { useDispatch } from 'react-redux';
import { useSelector } from 'react-redux';
import { setTasks } from '../../lib/features/global';
import { RootState } from '../../lib/store';
import { Task } from '../../types';

export default function TaskAdd() {

    // hooks
    const [title, setTitle] = useState<string>("");
    const [titleError, setTitleError] = useState<string>("");
    const [description, setDescription] = useState<string>("");

    const tasks = useSelector((state: RootState) => state.GlobalSlice.tasks);

    const dispatch = useDispatch();

    const resetFields = () => {
        setTitle("");
        setDescription("");
    }

    // to get last task id 
    const getLastTaskId = () => {
        if (tasks.length == 0)
            return -1
        else {
            return tasks[tasks.length - 1].id;
        }
    }

    // check validity of task title
    const checkValidation = () => {
        if (title.trim().length == 0) {
            return false
        }
        return true;
    }

    // add task to tasks in redux store
    const addTask = () => {
        setTitleError("");

        if (!checkValidation()) {
            setTitleError("Title cannot be empty");
            return;
        }

        const id = getLastTaskId() + 1

        let tempTask: Task = {
            id: id,
            title: title,
            description: description,
            isCompleted: false
        }
        dispatch(setTasks([...tasks, tempTask]));
        resetFields();
    }

    return (
        <Box sx={{ padding: "15px" }}>
            <form onSubmit={(e) => {
                e.preventDefault();
                addTask();
            }}>
                <TextField
                    onChange={(e) => setTitle(e.currentTarget.value)}
                    value={title}
                    fullWidth
                    sx={{ mb: "10px" }}
                    helperText={titleError}
                    error={titleError.length > 0}
                    placeholder="Enter the title"
                    label="Title" />
                <TextField
                    onChange={(e) => setDescription(e.currentTarget.value)}
                    value={description}
                    multiline
                    fullWidth
                    sx={{ mb: "10px" }}
                    placeholder="Enter the Description"
                    label="Description" />

                <Button type="submit" variant="contained" color="success">
                    Add Task
                </Button>
            </form>
        </Box>
    )
}
