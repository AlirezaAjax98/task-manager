import { Box, FormControl, InputLabel, MenuItem, Select, Button, Typography } from '@mui/material'
import { useState } from 'react'
import { useDispatch } from 'react-redux';
import { setFilterDoneState } from '../../lib/features/global';
import { FilterDoneState } from '../../types';

export default function FilterComponent() {

    const [filterIsCompleted, setFilterIsCompleted] = useState<FilterDoneState>("both");

    const dispatch = useDispatch();

    const applyFilter = () => {
        dispatch(setFilterDoneState(filterIsCompleted));
    }

    const handleChange = (value: FilterDoneState) => {
        setFilterIsCompleted(value);
    }

    return (
        <form onSubmit={(e) => {e.preventDefault();applyFilter();}}>
            <fieldset style={{ borderRadius: "5px",borderWidth : "1px" }}>
                <legend>
                    <Typography>
                        Filter
                    </Typography>
                </legend>
                <Box sx={{ display: "flex", justifyContent: "start", alignItems: "center" }}>
                    <FormControl sx={{ minWidth: "200px", mx: "10px" }}>
                        <InputLabel id="isCompletedSelect">Done</InputLabel>
                        <Select
                            labelId="isCompletedSelect"
                            value={filterIsCompleted}
                            label="Is Done"
                            onChange={(e) => handleChange(e.target.value as FilterDoneState)}
                        >
                            <MenuItem value={"done"}>Done</MenuItem>
                            <MenuItem value={"notDone"}>Not Done Yet</MenuItem>
                            <MenuItem value={"both"}>Both</MenuItem>
                        </Select>
                    </FormControl>
                    <Button type="submit" variant="contained" color="primary" >
                        Apply
                    </Button>
                </Box>
            </fieldset>
        </form>
    )
}
