import { Box } from '@mui/system'
import { useEffect } from 'react'
import { useDispatch } from 'react-redux';
import { useSelector } from 'react-redux'
import { setTasksForShow } from '../../lib/features/global';
import { RootState } from '../../lib/store'
import { Task } from '../../types';
import FilterComponent from '../FilterComponent';
import TaskList from '../TaskList';

export default function TaskContainer() {

    const tasks = useSelector((state: RootState) => state.GlobalSlice.tasks);
    const filterDoneState = useSelector((state: RootState) => state.GlobalSlice.filterDoneState)

    const dispatch = useDispatch();

    const filterTasks = () => {
        let tasksForShowTemp: Task[] = [];
        switch (filterDoneState) {
            case 'done':
                tasksForShowTemp = tasks.filter((task) => task.isCompleted == true);
                break;
            case 'notDone':
                tasksForShowTemp = tasks.filter((task) => task.isCompleted == false);
                break;
            case 'both':
                tasksForShowTemp = tasks;
                break;
        }
        dispatch(setTasksForShow([...tasksForShowTemp]));
    }

    useEffect(() => {
        filterTasks();
    }, [tasks, filterDoneState])

    return (
        <Box sx={{ height: "100vh", padding: "15px" }}>
            <FilterComponent />
            <TaskList />
        </Box>
    )
}
