import { Box, Button } from '@mui/material'
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";
import { useDispatch } from 'react-redux';
import { useSelector } from 'react-redux';
import { setTasks, setTasksForShow } from '../../lib/features/global';
import { RootState } from '../../lib/store';

export default function TaskList() {

    const dispatch = useDispatch();
    const tasksForShow = useSelector((state: RootState) => state.GlobalSlice.tasksForShow);
    const tasks = useSelector((state: RootState) => state.GlobalSlice.tasks);

    // Function to update list on drop
    const handleDrop = (droppedItem: any) => {
        // Ignore drop outside droppable container
        if (!droppedItem.destination) return;
        var updatedList = [...tasksForShow];
        // Remove dragged item
        const [reorderedItem] = updatedList.splice(droppedItem.source.index, 1);
        // Add dropped item
        updatedList.splice(droppedItem.destination.index, 0, reorderedItem);
        // Update State
        dispatch(setTasksForShow(updatedList))
    };

    const updateTaskCompletion = (id: number) => {
        const updatedTasks = tasks.map(task =>
            task.id === id ? { ...task, isCompleted: true } : task
        );
        return updatedTasks;
    };

    const completeTask = (id: number) => {
        const temp = updateTaskCompletion(id);
        dispatch(setTasks(temp));
    }

    return (
        <Box>
            <DragDropContext onDragEnd={handleDrop} >

                <Droppable droppableId="list-container">
                    {(provided) => (
                        <Box
                            className="list-container"
                            {...provided.droppableProps}
                            ref={provided.innerRef}
                        >
                            {
                                tasksForShow.map((item, index) => (
                                    <Draggable key={item.id} draggableId={item.id.toString()} index={index}>
                                        {(provided) => (
                                            <Box
                                                className="item-container"
                                                ref={provided.innerRef}
                                                {...provided.dragHandleProps}
                                                {...provided.draggableProps}
                                            >
                                                {item.title}
                                                <Box sx={{ fontSize: "14px" }}>
                                                    {item.description}
                                                </Box>
                                                <Box sx={{ display: "flex", justifyContent: "end" }}>
                                                    {
                                                        item.isCompleted ?
                                                            <img src="tick.png" width="20" />
                                                            :
                                                            <Button sx={{ color: "green" }} onClick={() => completeTask(item.id)} >
                                                                Mark as done
                                                            </Button>
                                                    }
                                                </Box>
                                            </Box>
                                        )}
                                    </Draggable>)
                                )
                            }
                        </Box>
                    )}
                </Droppable>
            </DragDropContext>
        </Box>
    )
}
