import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { FilterDoneState, Task } from "../../types";

export interface GlobalState {
    tasks : Task[]
    tasksForShow : Task[]
    filterDoneState : FilterDoneState
}

const initialState: GlobalState = {
    tasks : [],
    tasksForShow : [],
    filterDoneState : "both"
}

export const GlobalSlice = createSlice({
    name: "global",
    initialState: initialState,
    reducers: {
        setTasks : (state , action : PayloadAction<Task[]>) => {
            state.tasks = action.payload;
        },
        setFilterDoneState : (state , action : PayloadAction<FilterDoneState>) => {
            state.filterDoneState = action.payload;
        },
        setTasksForShow : (state , action : PayloadAction<Task[]>) => {
            state.tasksForShow = action.payload;
        },
    }
})

export const { setTasks,setTasksForShow,setFilterDoneState } = GlobalSlice.actions;
export default GlobalSlice.reducer;

